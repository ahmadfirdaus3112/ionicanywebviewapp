import { AppPage } from './app.po';
import {browser, ElementFinder, ElementHelper} from "protractor";

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display duckduckgo', () => {
    browser.waitForAngularEnabled(false)
    page.navigateTo();
    expect(page.getParagraphText()).toContain('DuckDuckGo');
  });
  it('should be able to enter searchtext', () => {
    browser.waitForAngularEnabled(false)
    page.navigateTo();
    let searchform:ElementFinder;
    searchform = page.getSearchForm();
    searchform.sendKeys('this is an automated search!');
  });
});
