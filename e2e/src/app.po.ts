import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.deepCss('div.tag-home__item')).getText();
  }

  getSearchForm(){
    return element(by.deepCss('input'));
  }
}
